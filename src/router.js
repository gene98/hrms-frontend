import Vue from 'vue';
import Router from 'vue-router';
import Status from './views/Status.vue';

Vue.use(Router);

export default new Router({
  routes: [{
      path: '/',
      redirect: '/status'
    },
    {
      path: '/status',
      name: '客房状态',
      component: Status
    },
    {
      path: '/reception/book',
      name: '房间预定',
      component: () =>
        import('./views/Reception/Book.vue')
    },
    {
      path: '/reception/check_in',
      name: '入住登记',
      component: () =>
        import('./views/Reception/CheckIn.vue')
    },{
      path: '/reception/check_in_record',
      name: '入住记录',
      component: () =>
        import('./views/Reception/CheckInRecord.vue')
    },{
      path: '/reception/client_messages',
      name: '客户留言',
      component: () =>
        import('./views/Reception/ClientMessages.vue')
    },{
      path: '/report_form/daily',
      name: '日报表',
      component: () =>
        import('./views/ReportForm/Daily.vue')
    },{
      path: '/report_form/weekly',
      name: '周报表',
      component: () =>
        import('./views/ReportForm/Weekly.vue')
    },{
      path: '/report_form/monthly',
      name: '月报表',
      component: () =>
        import('./views/ReportForm/Monthly.vue')
    }
  ]
});