var validateName = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('请输入姓名'))
  } else if (value.length < 2 || value.length > 5) {
    callback(new Error('长度在 2 到 5 个字符'))
  } else {
    callback()
  }
}
var validateSex = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('请选择性别'))
  } else {
    callback()
  }
}
var validateRoomType = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('请选择房间类型'))
  } else {
    callback()
  }
}
var validateRoomId = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('请选择房间号'))
  } else {
    callback()
  }
}
var validatorIdCard = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('请输入身份证号码'))
  } else if (!/(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$)/.test(value)) {
    callback(new Error('请输入正确的身份证号码'))
  } else {
    callback()
  }
}
var validateTelephone = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('请输入手机号码'))
  } else if (!/\d{11}/.test(value)) {
    callback(new Error('请输入正确的手机号码'))
  } else {
    callback()
  }
}
export {
  validateName,
  validateSex,
  validateRoomType,
  validateRoomId,
  validatorIdCard,
  validateTelephone,
}