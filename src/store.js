import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const testRooms = [{
    roomId: '201',
    floor: '2楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '202',
    floor: '2楼',
    type: '标准单人间',
    status: '预定'
  },
  {
    roomId: '203',
    floor: '2楼',
    type: '标准双人间',
    status: '住客'
  },
  {
    roomId: '204',
    floor: '2楼',
    type: '标准双人间',
    status: '维护'
  },
  {
    roomId: '205',
    floor: '2楼',
    type: '豪华单人间',
    status: '超时'
  },
  {
    roomId: '206',
    floor: '2楼',
    type: '豪华单人间',
    status: '空闲'
  },
  {
    roomId: '207',
    floor: '2楼',
    type: '豪华套房',
    status: '预定'
  },
  {
    roomId: '208',
    floor: '2楼',
    type: '豪华套房',
    status: '住客'
  },
  {
    roomId: '209',
    floor: '2楼',
    type: '棋牌房',
    status: '维护'
  },
  {
    roomId: '210',
    floor: '2楼',
    type: '棋牌房',
    status: '超时'
  },
  {
    roomId: '301',
    floor: '3楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '302',
    floor: '3楼',
    type: '标准单人间',
    status: '预定'
  },
  {
    roomId: '303',
    floor: '3楼',
    type: '标准双人间',
    status: '住客'
  },
  {
    roomId: '304',
    floor: '3楼',
    type: '标准双人间',
    status: '维护'
  },
  {
    roomId: '305',
    floor: '3楼',
    type: '豪华单人间',
    status: '超时'
  },
  {
    roomId: '306',
    floor: '3楼',
    type: '豪华单人间',
    status: '空闲'
  },
  {
    roomId: '307',
    floor: '3楼',
    type: '豪华套房',
    status: '预定'
  },
  {
    roomId: '308',
    floor: '3楼',
    type: '豪华套房',
    status: '住客'
  },
  {
    roomId: '309',
    floor: '3楼',
    type: '棋牌房',
    status: '维护'
  },
  {
    roomId: '310',
    floor: '3楼',
    type: '棋牌房',
    status: '超时'
  },
  {
    roomId: '401',
    floor: '4楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '402',
    floor: '4楼',
    type: '标准单人间',
    status: '预定'
  },
  {
    roomId: '403',
    floor: '4楼',
    type: '标准双人间',
    status: '住客'
  },
  {
    roomId: '404',
    floor: '4楼',
    type: '标准双人间',
    status: '维护'
  },
  {
    roomId: '405',
    floor: '4楼',
    type: '豪华单人间',
    status: '超时'
  },
  {
    roomId: '406',
    floor: '4楼',
    type: '豪华单人间',
    status: '空闲'
  },
  {
    roomId: '407',
    floor: '4楼',
    type: '豪华套房',
    status: '预定'
  },
  {
    roomId: '408',
    floor: '4楼',
    type: '豪华套房',
    status: '住客'
  },
  {
    roomId: '409',
    floor: '4楼',
    type: '棋牌房',
    status: '维护'
  },
  {
    roomId: '410',
    floor: '4楼',
    type: '棋牌房',
    status: '超时'
  },
  {
    roomId: '501',
    floor: '5楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '502',
    floor: '5楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '503',
    floor: '5楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '504',
    floor: '5楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '505',
    floor: '5楼',
    type: '标准单人间',
    status: '空闲'
  },
  {
    roomId: '506',
    floor: '5楼',
    type: '标准单人间',
    status: '空闲'
  }
];
const testOptions = {
  type: ['标准单人间', '标准双人间', '豪华单人间', '豪华套房', '棋牌房'],
  status: ['空闲', '预定', '住客', '维护', '超时'],
  floor: ['2楼', '3楼', '4楼', '5楼', '6楼']
};
const testCheckInRecord = [{
  "name": "胡靖",
  "sex": "男",
  "idCard": "430321199801010101",
  "roomType": "标准单人间",
  "roomId": "201",
  "periodNumber": 1,
  "periodUnit": "天",
  "startTime": new Date(2018, 8, 27, 15, 1, 14),
  "endTime": new Date(2018, 8, 28, 12, 0, 0),
  "leaveTime": new Date(2018, 8, 28, 11, 56, 34),
  "price": 100,
  "deposit": 100,
  "totalPrice": 200
}, {
  "name": "马毅",
  "sex": "男",
  "idCard": "430321199801010101",
  "roomType": "标准单人间",
  "roomId": "202",
  "periodNumber": 2,
  "periodUnit": "天",
  "startTime": new Date(2018, 8, 26, 15, 1, 14),
  "endTime": new Date(2018, 8, 28, 12, 0, 0),
  "leaveTime": new Date(2018, 8, 28, 11, 56, 34),
  "price": 200,
  "deposit": 100,
  "totalPrice": 300
}, {
  "name": "胡靖",
  "sex": "男",
  "idCard": "430321199801010101",
  "roomType": "豪华单人间",
  "roomId": "206",
  "periodNumber": 2,
  "periodUnit": "天",
  "startTime": new Date(2018, 8, 20, 14, 56, 14),
  "endTime": new Date(2018, 8, 22, 12, 0, 0),
  "leaveTime": new Date(2018, 8, 22, 11, 50, 34),
  "price": 240,
  "deposit": 100,
  "totalPrice": 340
}, {
  "name": "马毅",
  "sex": "男",
  "idCard": "430321199801010101",
  "roomType": "标准单人间",
  "roomId": "202",
  "periodNumber": 2,
  "periodUnit": "天",
  "startTime": new Date(2018, 8, 26, 15, 1, 14),
  "endTime": new Date(2018, 8, 28, 12, 0, 0),
  "leaveTime": new Date(2018, 8, 28, 11, 56, 34),
  "price": 200,
  "deposit": 100,
  "totalPrice": 300
}]
export default new Vuex.Store({
  state: {
    allRooms: testRooms,
    options: testOptions,
    checkedTags: [],
    countCheckedTags: {
      type: 0,
      status: 0,
      floor: 0
    },
    activeMenuItem: '客房状态',
    checkInRecord: testCheckInRecord,
    bookRecord: [],
    price: {
      '标准单人间': {
        'hour': 50,
        'accumulate': 15,
        'day': 100
      },
      '标准双人间': {
        'hour': 60,
        'accumulate': 15,
        'day': 110
      },
      '豪华单人间': {
        'hour': 70,
        'accumulate': 20,
        'day': 120
      },
      '棋牌房': {
        'hour': 70,
        'accumulate': 20,
        'day': 120
      },
      '豪华双人间': {
        'hour': 80,
        'accumulate': 25,
        'day': 130
      },
      '豪华套房': {
        'hour': 90,
        'accumulate': 25,
        'day': 140
      },
    },
    deposit: 100,
    clientMessages: [{
      time: new Date(2018, 8, 20, 12, 0),
      name: '胡靖',
      message: '服务不错',
      reply: '谢谢你的光临，欢迎下次再来'
    }]
  },
  mutations: {
    appendTag(state, obj) {
      state.checkedTags.push(obj);
      state.countCheckedTags[obj['type']] += 1;
    },
    removeTag(state, obj) {
      state.checkedTags.splice(state.checkedTags.indexOf(obj), 1);
      state.countCheckedTags[obj['type']] -= 1;
    },
    clearTags(state) {
      state.checkedTags = [];
      state.countCheckedTags = {
        type: 0,
        status: 0,
        floor: 0
      };
    },
    initAllRooms(state, rooms) {
      state.allRooms = rooms;
    },
    changeVisibility(state, obj) {
      state.allRooms[obj['index']]['hidden'] = obj['value'];
    },
    updateActiveMenuItem(state, item) {
      state.activeMenuItem = item
    },
    addChechInRecord(state, obj) {
      state.checkInRecord.push(obj)
    },
    addBookRecord(state, obj) {
      state.bookRecord.push(obj)
    },
    deleteClientMessage(state, obj) {
      state.clientMessages[obj['name']]['message'] = obj['message']
    },
    replyClient(state, obj) {
      state.clientMessages[obj['name']]['reply'] = obj['reply']
    }
  },
  actions: {}
});